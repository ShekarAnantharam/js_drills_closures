function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    
    
    function invoke(){
       
        for(let i=1;i<=n;i++){
            
            cb();
        }
    }
    return invoke;

}

module.exports=limitFunctionCallCount;