function counterFactory(counterVariable) {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    
    const obj= {increment:function(){return ++counterVariable;},
                 decrement:function(){return --counterVariable;}
}

return obj;

}
module.exports=counterFactory;
